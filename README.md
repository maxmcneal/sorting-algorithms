<div align="center">
    <h1>
        <img src="images/header.png" alt="Sorting Algorithms">
    </h1>
    <h4>
        Interactive python sorting algorithm visualizer using the <a href="https://github.com/pygame/pygame" target="_blank">PyGame</a> library. One of my very first projects.
    </h4>
</div>

<div align="center">
  <img src="https://img.shields.io/github/v/release/nhn/tui.editor.svg?include_prereleases">
  <img src="https://img.shields.io/github/license/nhn/tui.editor.svg">
  <img src="https://img.shields.io/badge/%3C%2F%3E%20with%20%E2%99%A5-ff1414.svg">
  <img src="https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&amp;style=flat">
</div>

<div align="center">
  <a href="#-goals-and-requirements">Goals and Requirements</a> •
  <a href="#-key-learnings">Key Learnings</a> •
  <a href="#-installation">Installation</a> •
  <a href="#-contributing">Contributing</a> •
  <a href="#-credits">Credits</a> •
  <a href="#-license">License</a>
</div>

<br/>

<div align="center">

![screenshot](images/example.gif)

</div>

## 🎯 Goals and Requirements

- Multiple sorting algorithms such as Quick Sort, Bubble Sort, Selection Sort, Merge Sort, and Heap Sort to visualize their sorting process
- Viewed at various speeds, ranging from instant to very slow, enabling detailed observation of the algorithm's functioning
- Size of the array being sorted can be adjusted, allowing for the visualization of sorting on different scales
- A timer displayed to track the time elapsed during the sorting process
- Reset the array or initiate sorting with simple button clicks
- Interactive buttons and controls to manipulate sorting settings like speed and array size.

## 📙 Key learnings

- In-depth understanding of various sorting algorithms like Quick Sort, Bubble Sort, Selection Sort, Merge Sort, and Heap Sort
- Learning to use the Pygame library for creating graphical user interfaces and handling events
- Acquiring skills in rendering graphics, such as drawing shapes and text, to visually represent data structures and algorithms
- Learning to measure and display algorithm performance in real-time, using timers
- Gaining experience in designing interactive and user-friendly interfaces with buttons and controls
- Understanding the implications of algorithm complexity and efficiency in real-world applications

## ⚙️ Installation

- TBA

## 🐛 Contributing

- TBA

## 💭 Credits

- me
- https://github.com/pygame/pygame

## 📜 License

This project is licensed under the terms of the MIT license and protected by Udacity Honor Code and Community Code of Conduct. See <a href="LICENSE.md">license</a> and <a href="LICENSE.DISCLAIMER.md">disclaimer</a>.
