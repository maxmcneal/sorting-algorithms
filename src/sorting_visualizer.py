import random
import math
import time

# https://github.com/pygame/pygame
import pygame

# sorting settings
array_size = 50
sort_speed = 2  # Instant    | Fast        | Normal         | Slow       | Very Slow 
algorithm = 0   # Quick Sort | Bubble Sort | Selection Sort | Merge Sort | Heap Sort

# pygame setup
pygame.init();
screen = pygame.display.set_mode((800, 630))

# draw window
pygame.display.set_caption("Sorting Algorithms - Max McNeal")
pygame.draw.rect(screen, (0, 0, 20), (0, 0, 800, 630))

elapsed_time, start_time = 0, 0

# initialize fonts
pygame.font.init()
arial_p12 = pygame.font.SysFont('Arial', 12)
arial_p14 = pygame.font.SysFont('Arial', 14)
impact_p24 = pygame.font.SysFont('Impact', 24)

# initialize colors
color_dark_blue = (0, 0, 30)
color_blue = (75, 122, 230)
color_light_blue = (105, 152, 255)
color_green = (75, 229, 137)
color_red = (229, 42, 60)
color_purple = (200, 0, 200)
colors = [color_blue for _ in range(array_size)]

circle_pos = [
    (15, 535),
    (15, 555),
    (15, 575),
    (15, 595),
    (15, 615),

    (105, 535),
    (105, 555),
    (105, 575),
    (105, 595),
    (105, 615),

    (200, 535),
    (200, 555),
    (200, 575),
    (200, 595),
    (200, 615)
]

button_pos = [
    (400, 530, 100, 50),
    (600, 530, 100, 50)
]

# get mouse positions
mouse_x, mouse_y = pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1]

# calculate line width
line_width = int(round((800 - (array_size - 1) * 5 - 2 - 5) / array_size))

# array of lines heights
arr = []

def populate_array():
    """
    The function "populate_array" generates random numbers between 10 and 500 and adds them to the "vet"
    array until it reaches the desired size.
    """
    while len(arr) < array_size:
        r = random.randint(10, 500)
        if r not in arr:
            arr.append(r)

def render_array():
    """
    The function `render_array` draws rectangles on a window using the values in the `arr` array as the
    heights of the rectangles.
    """
    index = 0
    line_x = 2 + index * (5 + line_width)

    for line in arr:
        pygame.draw.rect(screen, colors[index], (line_x, line - 1, line_width, 500 - line + 2))
        index += 1
        line_x = 2 + index * (5 + line_width)

    pygame.display.update()

def clear_screen():
    """
    The function `clear_screen` clears the screen by drawing a rectangle with a dark color over the
    entire screen.
    """
    pygame.draw.rect(screen, (0, 0, 20), (0, 0, 800, 501))

def refresh_lines():
    """
    The function refresh_lines initializes and updates the array, line width, and colors used for
    drawing lines.
    """
    clear_screen()
    
    global arr, line_width, colors

    # clear line height array
    arr = []

    # repopulate line height array
    while len(arr) < array_size:
        r2 = random.randint(10, 500)
        if r2 not in arr:
            arr.append(r2)

    # recalculate line width
    line_width = int(round((800 - (array_size - 1) * 5 - 2 - 5) / array_size))

    colors = [color_blue for _ in range(array_size)]

    for i2 in range(0, len(colors)):
        colors[i2] = color_blue

    render_array()

def swap_pos(pos1, pos2):
    """
    The function `swap_pos` swaps the positions of two elements in a global variable `vet`.
    
    Args:
      pos1: The position of the first element to be swapped in the array.
      pos2: The parameter `pos2` is the position of the element that you want to swap with `pos1`.
    """
    global vet
    arr[pos1], arr[pos2] = arr[pos2], arr[pos1]

def slow_down():
    """
    The function `slow_down` uses a `match` statement to delay the execution of code based on the value
    of `sort_speed`.
    """
    if sort_speed == 2:
        pygame.time.delay(5)
    if sort_speed == 3:
            pygame.time.delay(20)
    if sort_speed == 4:
        pygame.time.delay(100)

def quick_sort(array, low, high):
    """
    The function implements the quicksort algorithm to sort an array recursively, and also includes
    calls to clear the screen and render the array.
    
    Args:
      array: The array parameter is the list or array that you want to sort using the quick sort
    algorithm.
      low: The "low" parameter represents the starting index of the subarray that needs to be sorted.
      high: The "high" parameter in the quick_sort function represents the index of the highest element
    in the array that needs to be sorted.
    """
    if low < high:
        q = quick(array, low, high)
        quick_sort(array, low, q - 1)
        quick_sort(array, q + 1, high)
    clear_screen()
    render_array()
    
def quick(array, low, high):
    """
    The function "quick" implements the quicksort algorithm to sort an array by selecting a pivot
    element and partitioning the array into two sub-arrays, one with elements smaller than the pivot and
    one with elements greater than the pivot.
    
    Args:
      array: The array parameter is the list of elements that you want to sort using the quicksort
    algorithm.
      low: The parameter "low" represents the starting index of the subarray that needs to be sorted.
      high: The parameter "high" represents the index of the last element in the array or the subarray
    that we want to partition.
    
    Returns:
      the index of the pivot element after partitioning the array.
    """
    i = low-1
    pivot = array[high]
    for j in range(low, high):
        set_line_color(j, color_red)
        set_line_color(i+1, color_red)
        render_timer()
        slow_down()

        if array[j] >= pivot:
            i = i + 1
            set_line_color(i, color_blue)
            swap_pos(i, j)

        set_line_color(i+1, color_blue)
        set_line_color(j, color_blue)

        clear_screen()
        
        render_array()
    swap_pos(i+1, high)
    set_line_color(i+1, color_purple)
    return i+1

def illustrate_process():
    """
    The function illustrates the process of sorting an array using a visual representation.
    """
    sorted_arr = arr.copy()
    sorted_arr.sort(reverse=True)
    if arr == sorted_arr:
        for index in range(len(arr)):
            pygame.event.get()
            set_line_color(index, color_blue)
            slow_down()
        for index in range(len(arr)):
            pygame.event.get()
            if index+1 < len(arr):
                set_line_color(index+1, color_purple)
            set_line_color(index, color_purple)
            slow_down()
            set_line_color(index, color_green)

def sort():
    """
    The function `sort()` sorts an array using various sorting algorithms and visualizes the sorting
    process.
    """
    global start_time

    # start timer
    start_time = time.time()

    # instantly sort array
    if sort_speed == 0:
        vet.sort(reverse=True)
        for i2 in range(0, len(colors)):
            colors[i2] = color_green
        clear_screen()
        render_array()
        return

    # sort based on the algorithm selected
    if algorithm == 0:
        quick_sort(arr, 0, len(arr) - 1)
    if algorithm == 1:
        quick_sort(arr, 0, len(arr) - 1)
    if algorithm == 2:
        quick_sort(arr, 0, len(arr) - 1)
    if algorithm == 3:
        quick_sort(arr, 0, len(arr) - 1)
    if algorithm == 4:
        quick_sort(arr, 0, len(arr) - 1)

    # end timer
    start_time = 0

    # if were not doing a merge sort, visualize
    if algorithm != 3:
        illustrate_process()

def set_line_color(index, color):
    """
    The function sets the color of a line on a pygame window based on the given index and color.
    
    Args:
      index: The index parameter represents the index of the line in the vet list that you want to set
    the color for.
      color: The "color" parameter is the color that you want to set for the line. It should be a valid
    color value that can be recognized by the pygame library, such as a tuple representing RGB values
    (e.g., (255, 0, 0) for red) or a predefined color
    """
    line_x = 2 + index * (5 + line_width)
    pygame.draw.rect(screen, color, (line_x, arr[index] - 1, line_width, 500 - arr[index] + 2))
    pygame.display.update()
    colors[index] = color
    
def render_timer():
    """
    The function "render_timer" draws a rectangle and displays the elapsed time on the screen.
    """
    global elapsed_time
    pygame.event.get()
    elapsed_time = time.time() - start_time
    pygame.draw.rect(screen, (0, 0, 20), (640, 615, 160, 15))
    screen.blit(arial_p12.render('Elapsed time: ' + str(round(elapsed_time, 2)) + ' seconds', False, (255, 255, 255)), (640, 615))

def render_text():
    """
    The function `render_text()` renders text on the screen for a user interface.
    """
    screen.blit(arial_p14.render('Speed:', False, (255, 255, 255)), (5, 505))
    screen.blit(arial_p12.render('Instant', False, (255, 255, 255)), (25, 528))
    screen.blit(arial_p12.render('Fast', False, (255, 255, 255)), (25, 548)) 
    screen.blit(arial_p12.render('Normal', False, (255, 255, 255)), (25, 568)) 
    screen.blit(arial_p12.render('Slow', False, (255, 255, 255)), (25, 588)) 
    screen.blit(arial_p12.render('Very slow', False, (255, 255, 255)), (25, 608))  

    screen.blit(arial_p14.render('Array Size:', False, (255, 255, 255)), (95, 505))  
    screen.blit(arial_p12.render('Huge', False, (255, 255, 255)), (115, 528))  
    screen.blit(arial_p12.render('Big', False, (255, 255, 255)), (115, 548)) 
    screen.blit(arial_p12.render('Medium', False, (255, 255, 255)), (115, 568))  
    screen.blit(arial_p12.render('Small', False, (255, 255, 255)), (115, 588))
    screen.blit(arial_p12.render('Very small', False, (255, 255, 255)), (115, 608))  

    screen.blit(arial_p14.render('Algorithm:', False, (255, 255, 255)), (190, 505))
    screen.blit(arial_p12.render('Quick Sort', False, (255, 255, 255)), (210, 568))
    screen.blit(arial_p12.render('Bubble Sort', False, (255, 255, 255)), (210, 528)) 
    screen.blit(arial_p12.render('Selection Sort', False, (255, 255, 255)), (210, 548))
    screen.blit(arial_p12.render('Merge Sort', False, (255, 255, 255)), (210, 588))
    screen.blit(arial_p12.render('Heap Sort', False, (255, 255, 255)), (210, 608))

def render_circles():
    """
    The function `render_circles` draws circles on the screen based on certain conditions.
    """
    for num in range(len(circle_pos)):
        pygame.draw.circle(screen, (255, 255, 255), circle_pos[num], 5)  # main circle
        pygame.draw.circle(screen, (255, 255, 255), circle_pos[num], 3)  # smaller circle

    if sort_speed == 0:
        pygame.draw.circle(screen, (0, 0, 0), (circle_pos[0][0], circle_pos[0][1]), 3)
    elif sort_speed == 1:
        pygame.draw.circle(screen, (0, 0, 0), (circle_pos[1][0], circle_pos[1][1]), 3)
    elif sort_speed == 2:
        pygame.draw.circle(screen, (0, 0, 0), (circle_pos[2][0], circle_pos[2][1]), 3)
    elif sort_speed == 3:
        pygame.draw.circle(screen, (0, 0, 0), (circle_pos[3][0], circle_pos[3][1]), 3)
    elif sort_speed == 4:
        pygame.draw.circle(screen, (0, 0, 0), (circle_pos[4][0], circle_pos[4][1]), 3)

    if array_size == 100:
        pygame.draw.circle(screen, (0, 0, 0), (circle_pos[5][0], circle_pos[5][1]), 3)
    elif array_size == 80:
        pygame.draw.circle(screen, (0, 0, 0), (circle_pos[6][0], circle_pos[6][1]), 3)
    elif array_size == 40:
        pygame.draw.circle(screen, (0, 0, 0), (circle_pos[7][0], circle_pos[7][1]), 3)
    elif array_size == 20:
        pygame.draw.circle(screen, (0, 0, 0), (circle_pos[8][0], circle_pos[8][1]), 3)
    elif array_size == 10:
        pygame.draw.circle(screen, (0, 0, 0), (circle_pos[9][0], circle_pos[9][1]), 3)

    pygame.draw.circle(screen, (0, 0, 0), (circle_pos[algorithm+10][0], circle_pos[algorithm+10][1]), 3)

    pygame.display.update()

def check_distance(x, y, x2, y2):
    """
    The function calculates the distance between two points in a two-dimensional plane.
    
    Args:
      x: The x-coordinate of the first point.
      y: The y-coordinate of the first point.
      x2: The x-coordinate of the second point.
      y2: The y-coordinate of the second point.
    
    Returns:
      the distance between two points (x, y) and (x2, y2) using the distance formula.
    """
    return math.sqrt((x - x2) ** 2 + (y - y2) ** 2)

def mark_circle():
    """
    The function "mark_circle" allows the user to select the sorting speed and array size by clicking on
    circles on the screen.
    
    Returns:
      the value of the variable `num` or `num + 5` depending on the condition that is met.
    """
    global sort_speed, array_size, algorithm

    for num in range(5):  # loop through speed buttons
        if check_distance(mouse_x, mouse_y, int(circle_pos[num][0]), int(circle_pos[num][1])) < 5:
            sort_speed = num
            return num

    for num in range(5):  # loop through speed buttons
        if check_distance(mouse_x, mouse_y, int(circle_pos[num + 5][0]), int(circle_pos[num + 5][1])) < 5:
            if num + 5 == 5:
                array_size = 100
            elif num + 5 == 6:
                array_size = 80
            elif num + 5 == 7:
                array_size = 40
            elif num + 5 == 8:
                array_size = 20
            elif num + 5 == 9:
                array_size = 10
            refresh_lines()
            return num + 5

    for num in range(5):  # loop through algorithm buttons
        if check_distance(mouse_x, mouse_y, int(circle_pos[num + 10][0]), int(circle_pos[num + 10][1])) < 5:
            if num + 10 == 10:
                algorithm = 0
            elif num + 10 == 11:
                algorithm = 1
            elif num + 10 == 12:
                algorithm = 2
            elif num + 10 == 13:
                algorithm = 3
            elif num + 10 == 14:
                algorithm = 4
            return num + 10
    return False

def render_buttons():
    """
    The function `render_buttons` draws two buttons on the screen and updates the display.
    """
    if mark_button() == 1:
        pygame.draw.rect(screen, color_light_blue, button_pos[0])
        pygame.draw.rect(screen, color_blue, button_pos[1])
    elif mark_button() == 2:
        pygame.draw.rect(screen, color_light_blue, button_pos[1])
        pygame.draw.rect(screen, color_blue, button_pos[0])
    else:
        pygame.draw.rect(screen, color_blue, button_pos[0])
        pygame.draw.rect(screen, color_blue, button_pos[1])
    screen.blit(impact_p24.render('SORT!', False, color_dark_blue),
             (button_pos[0][0] + 20, button_pos[0][1] + 10))
    screen.blit(impact_p24.render('REFRESH', False, color_dark_blue),
             (button_pos[1][0] + 10, button_pos[1][1] + 10))
    pygame.display.update()

def mark_button():
    """
    The function `mark_button` checks if the mouse cursor is within the boundaries of any buttons and
    returns the index of the button if it is, otherwise it returns False.
    
    Returns:
      the index of the button that is being clicked on. If no button is being clicked on, it returns
    False.
    """
    global mouse_x, mouse_y

    mouse_x, mouse_y = pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1]

    for num in range(len(button_pos)):
        if button_pos[num][0] < mouse_x < button_pos[num][0] + button_pos[num][2] and \
                button_pos[num][1] < mouse_y < button_pos[num][1] + button_pos[num][3]:
            return num + 1

    return False

def reset_timer():
    """
    The function `reset_timer` resets the elapsed time and start time variables and then calls the
    `render_timer` function.
    """
    global elapsed_time, start_time
    elapsed_time = 0
    start_time = time.time()
    
    render_timer()

populate_array()
render_buttons()
render_array()
render_text()
render_circles()
reset_timer()

run = True
while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.MOUSEMOTION:
            render_buttons()
        if event.type == pygame.MOUSEBUTTONUP:
            pygame.event.get()
            mouse_x, mouse_y = pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1]
            mark_circle()
            render_circles()

            if mark_button() == 1:  # sort button pressed
                sort()

            elif mark_button() == 2:  # refresh button pressed
                reset_timer()
                refresh_lines()
                clear_screen()
                render_array()

pygame.quit()